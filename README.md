# Wonderful World
Second year C++ & OpenGL project of IMAC engineering school in Gustave Eiffel University, Champs-sur-Marne, France.
By Luc Pinguet, Éloïse Tassin, Alaric Rougnon-Glasson
# Usage
## Linux
### Only tested on Ubuntu 20.10 64bits, compiled in C++ 17
In project root directory, compile with : `cmake ./` then `make`.
Launch executable with `./bin/WonderfulWorld`
