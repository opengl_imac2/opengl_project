@startuml schema
    hide <<struct>> circle
    
    namespace glimac {
        class SDLWindowManager {
            + bool pollEvent(SDL_Event& e)
            + bool isKeyPressed(SDLKey key) const
            + bool isMouseButtonPressed(uint32_t button) const
            + bool isKeyPressed(SDLKey key) const
            + bool isMouseButtonPressed(uint32_t button) const
            + glm::ivec2 getMousePosition() const
            + void swapBuffers()
            + float getTime() const
        }
    }

    namespace gWorld {
        class TrackballCamera {
            - glm::vec3 _target
            - float _fDistance
            - float _fAngleX
            - float _fAngleY
            + void moveFront(float delta)
            + void rotateLeft(float degrees)
            + void rotateUp(float degrees)
            + glm::mat4 getViewMatrix() const noexcept
        }

        class CameraController {
            - gWorld::TrackballCamera _camera
            - bool _mouseFirst = true
            - float _mouseRelX = 0
            - float _mouseRelY = 0
            + void onEvent(const glimac::SDLWindowManager& windowManager, const SDL_Event& e)
        }

        class ShapeVertex <<struct>> {
            glm::vec3 position
            glm::vec3 normal
            glm::vec2 texCoords
        }
        
    }

    namespace openGL {
        class IndexBuffer {
            - GLuint _bufferID
            + GLuint getId() const
            + void bind()
            + void unbind()
            + void setData(const std::vector<glm::uvec3>& indices, const GLenum &drawType)
        }

        class Renderer {
            - GLuint _frameVAO = 0
            - GLuint _frameVBO
            - gWorld::CameraController _controller
            + void draw(const VertexArray& va, const GLsizei& vertexCount, const Shader& shader) const
            + void clear()
            + void cameraControl(const glimac::SDLWindowManager& windowManager, const SDL_Event& event)
            + void renderFrame()
            + glm::mat4 getRenderViewMatrix()
        }

        gWorld.CameraController<--openGL.Renderer

        class Shader {
            - GLuint _shaderId
            - GLuint _vsId
            - GLuint _fsId
            - std::unordered_map<std::string, GLint> _uniformLocationCache
            - bool m_compiled
            - GLuint compileShader(const GLenum &shaderType, const std::string &shaderStr)
            - std::string findFile(const char *filePath)
            - std::string shaderTypeStr(const GLenum &shaderType)
            - void attachShaderId(const char *shaderName, const GLuint id)
            + bool use() const
            + void stopUse() const
            + GLuint getID() const
            + GLint getUniform(const std::string &uniformName)
            .. Some setters ..
            + void setInt(const std::string &uniformName, int v)
            + void setFloat(const std::string &uniformName, float v)
            + void setVec2f(const std::string &uniformName, const glm::vec2 &v)
            + void setVec2f(const std::string &uniformName, const float &x, const float &y)
            + void setVec3f(const std::string &uniformName, const glm::vec3 &v)
            + void setVec3f(const std::string &uniformName, const float &x, const float &y, const float &z)
            + void setVec4f(const std::string &uniformName, const glm::vec4 &v)
            + void setVec4f(const std::string &uniformName, const float &a, const float &b, const float &c, const float &d)
            + void setMat3(const std::string &uniformName, const glm::mat3 &mat)
            + void setMat4(const std::string &uniformName, const glm::mat4 &mat)    
        }

        class VertexArray {
            - GLuint _bufferID
            + void bind() const
            + void unbind() const
            + void addBuffer(const VertexBuffer& vb, const VertexBufferLayout& layout)
        }

        class VertexBuffer {
            - GLuint _bufferID
            + GLuint getId() const
            + void bind() const
            + void unbind() const
            + void setData(const std::vector<T>& data, const GLenum& drawType)
        }

        class VertexBufferLayout {
            - std::vector<vbElements> _elements
            - unsigned int _stride = 0
            + void setup(std::initializer_list<vbElements> const & elements)
            + const std::vector<vbElements> getElements() const
            + unsigned int getStride() const
        }

        class vbElements <<struct>> {
            unsigned int _type
            unsigned int _count
            unsigned char _normalized
        }

        vbElements<--VertexBufferLayout

        class GBuffer {
            - GLuint _bufferID
            - GLuint _gPosition
            - GLuint _gNormal
            - GLuint _gAlbedoSpec
            - GLuint _depthTexture
            + bool init(unsigned int windowWidth, unsigned int windowHeight)
            + void bindWriting() const
            + void bindReading() const
            + void bindTextures() const
        }

        enum TextType {
            DIFFUSE = 0
            SPECULAR
            NORMAL
            ROUGHNESS
            HEIGHTMAP
            DENSITY
        }

        class Imgtexture {
            - unsigned int _texID
            - std::string _filePath
            - unsigned char* _localBuffer
            - int _width,_height, _BPP
            - TextType _type
            + void bind(unsigned int slot = 0) const
            + void unbind() const
            + unsigned int getTexID()
        }

        TextType<--Imgtexture
    }

    ' namespace glDebug {
    '     void clearErrors()
    '     bool checkErrors(const char* functionName, const char* filename, int line)
    '     char const* errorString(GLenum const err)
    ' }

    class Vertex <<struct>> {
        glm::vec3 Position
        glm::vec2 TexCoords
        glm::vec3 Normal
    }

    class Texture <<struct>> {
        unsigned int id
        std::string type
        std::string path
    }

    class Mesh {
        - unsigned int VAO
        - unsigned int VBO
        - unsigned int EBO
        - void setupMesh()
        + std::vector<Vertex> _vertices
        + std::vector<unsigned int> _indices
        + std::vector<Texture> _textures
    }

    Vertex<--Mesh
    Texture<--Mesh

    class Model {
        - std::vector<Mesh> meshes
        - std::string directory
        - void loadModel(std::string path)
        - void processNode(aiNode *node, const aiScene *scene)
        - Mesh processMesh(aiMesh *mesh, const aiScene *scene)
        - std::vector<Texture> loadMaterialTextures(aiMaterial *mat, aiTextureType type, std::string typeName)
        + Draw(openGL::Shader &shader)
    }

    Mesh<--Model    

@enduml