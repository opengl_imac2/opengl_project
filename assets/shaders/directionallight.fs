#version 330
precision mediump float;

uniform vec3 uLightPos;
uniform vec3 uViewPos;

uniform sampler2D uTexture;
//uniform sampler2D uTexture2;

in vec3 vPosition_vs;
in vec3 vNormal_vs;
in vec2 vTexCoords;

out vec4 fFragColor;
//float uShininess = 0.1;

void main() {
    vec3 earthColor = texture(uTexture, vTexCoords).rgb;

    vec3 color = earthColor;
    //vec3 color = vec3(0.5);
    // ambient
    vec3 ambient = 0.15 * color;
    // diffuse
    vec3 lightDir = normalize(uLightPos - vPosition_vs);
    vec3 normal = normalize(vNormal_vs);
    float diff = max(dot(lightDir, normal), 0.0);
    vec3 diffuse = diff * color;
    // specular
    vec3 viewDir = normalize(- vPosition_vs);
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec;

    vec3 halfwayDir = normalize(lightDir + viewDir);  
    spec = pow(max(dot(normal, halfwayDir), 0.0), 1.0);
    
    
    vec3 specular = vec3(0.3) * spec; // assuming bright white light color
    fFragColor = vec4(1,0.8,0.6, 1) * vec4(ambient + diffuse + specular, 1.0);
}