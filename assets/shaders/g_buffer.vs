#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec2 aTexCoords;


out vec3 FragPos;
out vec2 TexCoords;
out vec3 Normal;

uniform mat4 uModelMatrix;
uniform mat4 uViewMatrix;
uniform mat4 uProjMatrix;

void main()
{
    vec4 worldPos = uModelMatrix * vec4(aPos, 1.0);
    FragPos = vec3(worldPos); 
    TexCoords = aTexCoords;
    
    mat3 normalMatrix = transpose(inverse(mat3(uModelMatrix)));
    Normal = normalMatrix * aNormal;

    gl_Position = uProjMatrix * uViewMatrix * worldPos;
}