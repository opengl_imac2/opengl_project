#version 330 core
layout (location = 0) out vec3 gPosition;
layout (location = 1) out vec4 gNormalMetallic;
layout (location = 2) out vec4 gDiffuseRoughness;

in vec2 TexCoords;
in vec3 FragPos;
in vec3 Normal;

out vec3 FragColor;

uniform sampler2D texture_diffuse1;
//uniform sampler2D texture_specular1;

void main()
{    
    
    gPosition = FragPos; // stock frag pos
    
    gNormalMetallic.rgb = normalize(Normal);// stock per-frag norm
  
    gNormalMetallic.a = 1.0f;
    gDiffuseRoughness.rgb = vec3(0.8f,0.8f,0.8f);
    // store specular intensity in gAlbedoSpec's alpha component
    gDiffuseRoughness.a = 0.1f;
    //gAlbedoSpec.a = 80;
}