#version 330

precision mediump float;

in vec2 vTexCoords;
in vec3 vNormal_vs;
in vec3 vPosition_vs;

out vec3 fFragColor;

void main() {
    fFragColor = normalize(vNormal_vs);
    //fFragColor = vec3(1.0f);
} 