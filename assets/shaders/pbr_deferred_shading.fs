

#version 330 core
out vec4 FragColor;
in vec2 TexCoords;

// material parameters
uniform sampler2D gPosition;
uniform sampler2D gNormalMetallic;
uniform sampler2D gDiffuseRoughness;

// lights
const int NBR_LIGHTS = 3;

uniform vec3 lightPositions[NBR_LIGHTS];
uniform vec3 lightColors[NBR_LIGHTS];
uniform vec3 lightStrength[NBR_LIGHTS];

uniform vec3 uViewPos;

const float PI = 3.14159265359;

float DistributionGGX(vec3 N, vec3 H, float roughness) // Distribution Term of specular calculation with GGX approx
{
    float a = roughness*roughness;
    float a2 = a*a;
    float NdotH = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;

    float nom   = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;

    return nom / denom;
}
// ----------------------------------------------------------------------------
float GeometrySchlickGGX(float NdotV, float roughness) // Geometry Term of specular calculation with Schilck approx
{
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float nom   = NdotV;
    float denom = NdotV * (1.0 - k) + k;

    return nom / denom;
}
// ----------------------------------------------------------------------------
float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness) // Sum of geometry terms of specular calculation with Schilck approx
{
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx2 = GeometrySchlickGGX(NdotV, roughness);
    float ggx1 = GeometrySchlickGGX(NdotL, roughness);

    return ggx1 * ggx2;
}
// ----------------------------------------------------------------------------
vec3 fresnelSchlick(float cosTheta, vec3 F0) //Fresnel Term of specular calculation with schilck approx; 
{
    return F0 + (1.0 - F0) * pow(max(1.0 - cosTheta, 0.0), 5.0);
}
// ----------------------------------------------------------------------------
void main()
{		
    vec3 albedo     = pow(texture(gDiffuseRoughness, TexCoords).rgb, vec3(2.2));
    float metallic  = texture(gNormalMetallic, TexCoords).a;
    float roughness = max(texture(gDiffuseRoughness, TexCoords).a, 0.05f);
    vec3 N = texture(gNormalMetallic, TexCoords).rgb;
    if(N == vec3(0.0f,0.0f,0.0f)){
        discard;
    }
    
    vec3 P = texture(gPosition, TexCoords).rgb;
    //float ao        = texture(aoMap, TexCoords).r;
    vec3 V = normalize(uViewPos - P);


    vec3 F0 = vec3(0.03); 
    F0 = mix(F0, albedo, metallic); //Real specular color (interpolation between metallic or not metallic. 0.04 is a default value for non-metallic material. Act like a )

    // reflectance equation
    vec3 Lo = vec3(0.0);
    for(int i = 0; i < NBR_LIGHTS; ++i) 
    {
        vec3 lightColor = lightColors[i]*255;
        // calculate per-light radiance
        vec3 L = normalize(lightPositions[i] - P);
        vec3 H = normalize(V + L);
        float distance = length(lightPositions[i] - P);
        float attenuation = 1.0 / (distance * distance);
        vec3 radiance = lightColor * attenuation;

        // Schlick BRDF
        float NDF = DistributionGGX(N, H, roughness);   
        float G = GeometrySmith(N, V, L, roughness);      
        vec3 F = fresnelSchlick(max(dot(H, V), 0.0), F0);
           
        vec3 nominator    = NDF * G * F; 
        float denominator = 4 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0) + 0.001; // 0.001 to prevent divide by zero.
        vec3 specular = nominator / denominator;
        
        // kS is equal to Fresnel
        vec3 kS = F;
        // for energy conservation, the diffuse and specular light can't
        // be above 1.0 (unless the surface emits light); to preserve this
        // relationship the diffuse component (kD) should equal 1.0 - kS.
        vec3 kD = vec3(1.0) - kS;
        // multiply kD by the inverse metalness such that only non-metals 
        // have diffuse lighting, or a linear blend if partly metal (pure metals
        // have no diffuse light).
        kD *= 1.0 - metallic;	  

        // scale light by NdotL
        float NdotL = max(dot(N, L), 0.0);        

        // add to outgoing radiance Lo
        Lo += 10*((kD * albedo / PI + specular) * radiance * NdotL);  // already multiplied the BRDF by the Fresnel (kS) so we won't multiply by kS again
    }   
    
    // ambient lighting (note that the next IBL tutorial will replace 
    // this ambient lighting with environment lighting).
    vec3 ambient = vec3(0.03) * albedo;
    
    vec3 color = ambient + Lo;

    // HDR tonemapping
    color = color / (color + vec3(1.0));
    // gamma correct
    color = pow(color, vec3(1.0/2.2)); 

    FragColor = vec4(color, 1.0);
}

