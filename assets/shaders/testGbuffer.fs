#version 330 core
out vec3 FragColor;

in vec2 TexCoords;

uniform sampler2D gPosition;
uniform sampler2D gNormalMetallic;
uniform sampler2D gDiffuseRoughness;

void main()
{   
    FragColor =  vec3(1.0f,texture(gDiffuseRoughness, TexCoords).a, texture(gNormalMetallic, TexCoords).a);
}