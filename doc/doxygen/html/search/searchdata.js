var indexSectionsWithContent =
{
  0: "abcgipsuv~",
  1: "isv",
  2: "g",
  3: "acs",
  4: "abgipsuv~",
  5: "v"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "groups"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Modules"
};

