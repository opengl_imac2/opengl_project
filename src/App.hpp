#pragma once

/**
 * @file App.hpp
 * @brief Import all the useful .hpp
 * 
 * 
 */

#include <GL/glew.h>
#include "glm.hpp"
#include <SDL/SDL.h>

#include "SDLWindowManager/SDLWindowManager.hpp"
#include "openGL/OpenGL_Manager.hpp"
#include "gameWorld/Sphere.hpp"
#include "gameWorld/Light.hpp"
#include "gameWorld/playerCharacter.hpp"
#include "models/models.hpp"