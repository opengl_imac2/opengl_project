#pragma once



#include <cstdint>
#include <SDL/SDL.h>

#include "glm/glm.hpp"

namespace glimac {
/**
 * @brief Encapsulate the SDL functions
 * 
 */
class SDLWindowManager {
public:
    /**
     * @brief Construct a new SDLWindowManager object
     * 
     * @param width 
     * @param height 
     * @param title 
     */
    SDLWindowManager(uint32_t width, uint32_t height, const char* title);
    /**
     * @brief Destroy the SDLWindowManager object
     * 
     */
    ~SDLWindowManager();

    /**
     * @brief Polls for currently pending events
     * 
     * @param e 
     * @return true if there are any pending events
     * @return false if there are none available
     */
    bool pollEvent(SDL_Event& e);
    /**
     * @brief 
     * 
     * @param key 
     * @return true if a key is pressed
     * @return false if no key is pressed
     */
    bool isKeyPressed(SDLKey key) const;

    /**
     * @brief button can SDL_BUTTON_LEFT, SDL_BUTTON_RIGHT and SDL_BUTTON_MIDDLE
     * 
     * @param button 
     * @return true 
     * @return false 
     */
    bool isMouseButtonPressed(uint32_t button) const;

    /**
     * @brief Get the Mouse Position object
     * 
     * @return glm::ivec2 
     */
    glm::ivec2 getMousePosition() const;

    /**
     * @brief Swap the OpenGL buffers, if double-buffering is supported
     * 
     */
    void swapBuffers();

    /**
     * @brief Get the Time object
     * 
     * @return float time in seconds
     */
    float getTime() const;
};

}
