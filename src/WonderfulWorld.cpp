#include <iostream>
#include <GL/glew.h>

#include "glm/glm.hpp"
#include "App.hpp"
#include "gameWorld/Box.hpp" //only for tests !

float WINDOW_W = 1000.0f;
float WINDOW_H = 1000.0f;

bool world = 0;

int main()
{
    std::cout << "------------- OpenGL Project -----------" << std::endl;

    glimac::SDLWindowManager windowManager(WINDOW_W, WINDOW_H, "WonderfulWorld");
    GLenum glewInitError = glewInit();
    if(GLEW_OK != glewInitError) {
        std::cerr << glewGetErrorString(glewInitError) << std::endl;
        return EXIT_FAILURE;
    }

    std::cout << "OpenGL Version : " << glGetString(GL_VERSION) << std::endl;
    std::cout << "GLEW Version : " << glewGetString(GLEW_VERSION) << std::endl;

    openGL::Shader testShader("assets/shaders/basic.vs", "assets/shaders/directionallight.fs");
    openGL::Shader geometryPass("assets/shaders/g_buffer.vs", "assets/shaders/g_buffer.fs");
    openGL::Shader geometryPassSimple("assets/shaders/g_buffer.vs", "assets/shaders/g_buffer_simple.fs");
    openGL::Shader lightingPass("assets/shaders/deferred_shading.vs", "assets/shaders/pbr_deferred_shading.fs");
    openGL::Shader shaderLightBox("assets/shaders/basic.vs", "assets/shaders/colors.fs");

    glEnable(GL_DEPTH_TEST);

    gWorld::playerCharacter mainChara;
    Model terrain = Model("assets/models/terrain/terrain.gltf");
    Model object = Model("assets/models/cube.gltf");
    gWorld::Box cube(5,5,5);

    gWorld::Sphere sphereTest(1,32,16);
    openGL::VertexBuffer vbo;
    vbo.setData(sphereTest.getVerticesData(),GL_STATIC_DRAW);
    

    openGL::VertexBufferLayout vblayout;
    openGL::VertexArray vao;
    openGL::Renderer renderer(WINDOW_W,WINDOW_H, &mainChara);

    const openGL::vbElements VERTEX_ATTR_POSITION = {GL_FLOAT,3,GL_FALSE};
    const openGL::vbElements VERTEX_ATTR_NORMAL = {GL_FLOAT,3,GL_FALSE};
    const openGL::vbElements VERTEX_ATTR_TEXTURE = {GL_FLOAT,2,GL_FALSE};
   

    vblayout.setup({VERTEX_ATTR_POSITION, VERTEX_ATTR_NORMAL, VERTEX_ATTR_TEXTURE});
    vao.addBuffer(vbo, vblayout);
    vao.unbind();

    openGL::ImgTexture moonMap("assets/textures/MoonMap.jpg");
    openGL::ImgTexture earthMap("assets/textures/EarthMap.jpg");

    glm::mat4 ProjMatrix = glm::perspective(glm::radians(70.f), WINDOW_W/WINDOW_H, 0.1f,100.0f);
    //glm::mat4 MVMatrix = glm::translate(renderer.getRenderViewMatrix(), glm::vec3(0.f,0.f,0.0f));
    
    glm::mat4 ModelMatrix = glm::mat4(1.0f);

    const unsigned int NBR_LIGHTS = 1;
    Light sunlight(glm::vec3(-10.0f,50.0f,5.0f), glm::vec3(1.0f,1.0f,1.0f), 1.0f);
    //Light sdlight(glm::vec3(2.0f,1.0f,0.0f), glm::vec3(1.0f,1.0f,1.0f), 1.0f);
    std::vector<glm::vec3>lightPositions;
    std::vector<glm::vec3>lightColors;

    lightPositions.push_back(sunlight.getPos());
    lightColors.push_back(sunlight.getColor());
    // lightPositions.push_back(sdlight.getPos());
    // lightColors.push_back(sdlight.getColor());

    lightingPass.use();
    lightingPass.setInt("gPosition", 0);
    lightingPass.setInt("gNormalMetallic", 1);
    lightingPass.setInt("gDiffuseRoughness", 2);

    bool done = false;
    while (!done) {
            // Event loop:
            SDL_Event e;
            while (windowManager.pollEvent(e)) {
                if (e.type == SDL_QUIT) {
                    done = true; // Leave the loop after this iteration
                }
                renderer.cameraControl(windowManager,e);
            }
            renderer.charaControl(e);
            renderer.updateCameraTarget(mainChara.getPos());

            // ----TESTS COLLISION----
            if (mainChara.box().collision(cube)) {
                mainChara.setBlocked(true);
                mainChara.setBlockedDirection(mainChara.box().side(cube));
            } else {
                mainChara.setBlocked(false);
            }
            // ------------------------            

            // Deferred Rendering (Draw Scene)
            renderer.geometryPass(geometryPass);

            // ----DrawCall Main Chara----
            ModelMatrix = glm::mat4(1.0f);
            ModelMatrix = glm::translate(ModelMatrix, mainChara.getPos());
            ModelMatrix = glm::translate(ModelMatrix, glm::vec3(0.0f, sin(windowManager.getTime()*2)/4, 0.0f));
            ModelMatrix = glm::rotate(ModelMatrix, -90.0f , glm::vec3(0, 1, 0)); // Translation * Rotation
            geometryPass.setMat4("uModelMatrix", ModelMatrix);
            mainChara.draw(geometryPass);

            // ----DrawCall terrain----
            ModelMatrix = glm::mat4(1.0f);
            ModelMatrix = glm::translate(ModelMatrix, glm::vec3(0,-3,0));
            geometryPass.setMat4("uModelMatrix", ModelMatrix);
            terrain.Draw(geometryPass);

            renderer.getGBuffer().unbind();

            renderer.lightingPass(lightingPass,glm::vec3(glm::inverse(renderer.getRenderViewMatrix())*glm::vec4(mainChara.getPos(),1.0)), lightPositions, lightColors);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            glEnable(GL_BLEND);
            
            // Forward Rendering (Draw lightPos)
            shaderLightBox.use();
            shaderLightBox.setMat4("uProjMatrix", ProjMatrix);
            shaderLightBox.setMat4("uViewMatrix", renderer.getRenderViewMatrix());

            for (unsigned int i = 0; i < lightPositions.size(); i++)
            {
                ModelMatrix = glm::mat4(1.0f);
                ModelMatrix = glm::translate(ModelMatrix, lightPositions[i]);
                ModelMatrix = glm::scale(ModelMatrix, glm::vec3(0.125f));
                shaderLightBox.setMat4("uModelMatrix", ModelMatrix);
                shaderLightBox.setVec3f("lightColor", lightColors[i]);
                renderer.renderCube();
            }
            // ----Test Collision box----
            // -> boîte fixe
            ModelMatrix = glm::mat4(1.0f);
            ModelMatrix = glm::translate(ModelMatrix, glm::vec3(7,0,5));
            ModelMatrix = glm::scale(ModelMatrix, glm::vec3(5.f));
            shaderLightBox.setMat4("uModelMatrix", ModelMatrix);
            shaderLightBox.setVec3f("lightColor", mainChara.getPos());
            renderer.renderCube();
            // -> pour le main chara
            ModelMatrix = glm::mat4(1.0f);
            ModelMatrix = glm::translate(ModelMatrix, mainChara.getPos());
            // ModelMatrix = glm::scale(ModelMatrix, glm::vec3(0.5f));
            shaderLightBox.setMat4("uModelMatrix", ModelMatrix);
            shaderLightBox.setVec3f("lightColor", glm::vec3(1.f));
            renderer.renderCube();
            // --------------------------
            windowManager.swapBuffers();
        }
    return EXIT_SUCCESS;
}
