#include "Box.hpp"
#include <stdlib.h> // abs

namespace gWorld {

Box::Box(float width, float height, float depth)
    : _width(width), _height(height), _depth(depth), _x(7), _y(0), _z(5)
{}

Box::~Box() {}

const bool Box::collision(const Box box) {
  return (_x - _width <= box._x + box._width && _x + _width >= box._x - box._width) && // left&right
         (_y - _height <= box._y + box._height && _y + _height >= box._y - box._height) && // up&down
         (_z - _depth <= box._z + box._depth && _z + _depth >= box._z - box._depth); // front&back
}

const glm::vec3 Box::side(const Box box) {
    if (abs((_x - _width) - (box._x + box._width)) <= 0.1) // Obstacle on the left
        return glm::vec3(-1,0,0);
    if (abs((_x + _width) - (box._x - box._width)) <= 0.1) // Obstacle on the right
        return glm::vec3(1,0,0);
    if (abs((_z + _depth) - (box._z - box._depth)) <= 0.1) // Obstacle on the back
        return glm::vec3(0,0,1);
    if (abs((_z - _depth) - (box._z + box._depth)) <= 0.1) // Obstacle on the front
        return glm::vec3(0,0,-1);
}

}
