#pragma once
#include "glm/glm.hpp"

namespace gWorld {


/**
 * @brief Used to handle collisions
 * 
 */
class Box {
    public:
        /**
         * @brief Construct a new Box object
         * 
         * @param width 
         * @param height 
         * @param depth 
         */
        Box(float width, float height, float depth);

        /**
         * @brief Destroy the Box object
         * 
         */
        ~Box();

        /**
         * @brief Check if there is a collision between this box and the one passed in parameters
         * 
         * @param box The box you want to check if there is a collision with
         * @return true 
         * @return false 
         */
        const bool collision(const Box box);
        
        /**
         * @brief Gives the direction the box collides towards in the world coordinates system
         * 
         * @param box 
         * @return const glm::vec3 : (0,0,1) if in the back, (0,0,-1) if on the front, (1,0,0) if on the right, (-1,0,0) if on the left
         */
        const glm::vec3 side(const Box box);

        /**
         * @brief Set the Coordinates object used for test, ask Elo if useful during code refactoring
         * 
         * @param x 
         * @param y 
         * @param z 
         */
        inline void setCoordinates(const float x, const float y, const float z) { _x = x; _y = y; _z = z;}

        //--> useless for now, if still here during code refactoring --> delete
        // inline float width() { return _width; }
        // inline float height() { return _height; }
        // inline float depth() { return _depth; }

    private:
        float _x, _y, _z; // center of the box
        float _width, _height, _depth;
};

}