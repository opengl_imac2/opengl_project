#include "CameraController.hpp"

namespace gWorld {

CameraController::CameraController(){}
CameraController::~CameraController(){}

void CameraController::onEvent(const glimac::SDLWindowManager& windowManager, const SDL_Event& event) {
    if (event.type == SDL_MOUSEBUTTONUP) {
        if (event.button.button == SDL_BUTTON_WHEELUP) {
            _camera.moveFront(-0.1);
        }
        else if (event.button.button == SDL_BUTTON_WHEELDOWN) {
            _camera.moveFront(0.1);
        }
    }
    if (windowManager.isMouseButtonPressed(SDL_BUTTON_LEFT)) {
        if (_mouseFirst) {
            _mouseFirst = false;
            _mouseRelX = windowManager.getMousePosition().x;
            _mouseRelY = windowManager.getMousePosition().y;
        }
        else {
            _camera.rotateLeft((_mouseRelY - windowManager.getMousePosition().y)/100);
            _camera.rotateUp((_mouseRelX - windowManager.getMousePosition().x)/100);
            _mouseRelX = windowManager.getMousePosition().x;
            _mouseRelY = windowManager.getMousePosition().y;
        }
    }
    else {
        _mouseFirst = true;
    }
}

    void CameraController::updateTarget(const glm::vec3 position){
        _camera.setTarget(position);
    }
};