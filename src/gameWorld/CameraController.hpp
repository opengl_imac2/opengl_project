#pragma once

#include <GL/glew.h>
#include "../openGL/Shader.hpp"
#include <glm/glm.hpp>
#include "commonAttributes.hpp"
#include "TrackballCamera.hpp"
#include "../SDLWindowManager/SDLWindowManager.hpp"

namespace gWorld {  
/**
 * @brief Encapsulates the controls of the camera
 * 
 */
class CameraController {
    public:
        /**
         * @brief Construct a new Camera Controller object
         * 
         */
        CameraController();
        /**
         * @brief Destroy the Camera Controller object
         * 
         */
        ~CameraController();

        // void onUpdate(); // On peut mettre le contenu de onEvent sur onUpdate et faire de onEvent un dispatcher en fonction du type d'action
        /**
         * @brief Handle all sort of events to make the use of the camera easier
         * 
         * @param windowManager 
         * @param e 
         */
        void onEvent(const glimac::SDLWindowManager& windowManager, const SDL_Event& e);

        void updateTarget(const glm::vec3 position);
        /**
         * @brief Get the View Matrix object, useful for the renderer
         * 
         * @return glm::mat4 
         */
        inline glm::mat4 getViewMatrix() const noexcept {return _camera.getViewMatrix();};

        inline TrackballCamera* camera(){ return &_camera; }


    private:
        // glimac::SDLWindowManager& _windowManager;
        gWorld::TrackballCamera _camera;
        bool _mouseFirst = true;
        float _mouseRelX = 0;
        float _mouseRelY = 0;

        // void onMouseScrolled(const SDL_Event& e);
        // void onWindowResized(const SDL_Event& e);
    

};

} // namespace gWorld