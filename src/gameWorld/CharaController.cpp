#include "CharaController.hpp"

namespace gWorld {

CharaController::CharaController(playerCharacter* chara, TrackballCamera* cam)
    : _character(chara), _camera(cam)
{}
CharaController::~CharaController(){}

void CharaController::onEvent(const SDL_Event& event) {
    Uint8 *keystate = SDL_GetKeyState(NULL); // so we can handle when a key is held down
    if (keystate[SDLK_q] || keystate[SDLK_z] || keystate[SDLK_s] || keystate[SDLK_d]) {
        if (keystate[SDLK_q])
            _character->move(LEFT,_camera->getViewMatrix());
        if (keystate[SDLK_z])
            _character->move(FRONT,_camera->getViewMatrix());
        if (keystate[SDLK_d])
            _character->move(RIGHT,_camera->getViewMatrix());
        if (keystate[SDLK_s])
            _character->move(BACK,_camera->getViewMatrix());
    }
}

}