#pragma once

#include <GL/glew.h>
#include "../openGL/Shader.hpp"
#include <glm/glm.hpp>
#include "commonAttributes.hpp"
#include "playerCharacter.hpp"
#include "../SDLWindowManager/SDLWindowManager.hpp" // to manage events
#include "TrackballCamera.hpp"

namespace gWorld {
/**
 * @brief to control the movement of the main character
 * 
 */
class CharaController {
    public:
        /**
         * @brief Construct a new Chara Controller object
         * 
         * @param chara the character to be controlled
         */
        CharaController(playerCharacter* chara, TrackballCamera* cam);

        /**
         * @brief Destroy the Chara Controller object
         * 
         */
        ~CharaController();

        /**
         * @brief Handle the classic zqsd to move the character
         * 
         * @param e to handle interaction (not implemented yet)
         */
        void onEvent(const SDL_Event& e);

    private:
        /**
         * @brief pointer to the character being controlled
         * 
         */
        gWorld::playerCharacter* _character;

        TrackballCamera* _camera;
};
}