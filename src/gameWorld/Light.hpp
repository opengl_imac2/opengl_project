#pragma once

#include <glm/glm.hpp>

class Light 
{
private:
    glm::vec3 _position;
    glm::vec3 _color;
    float _strength;

public:
    Light (glm::vec3 position, glm::vec3 color, float strength);
    ~Light ();

    inline const glm::vec3 getPos() const{ return _position;};
    inline const glm::vec3 getColor() const{ return _color;};
};