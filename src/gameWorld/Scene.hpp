#pragma once

#include <vector>
#include "Light.hpp"
#include "../openGL/Shader.hpp"

class Scene
{
    private:
    //std::vector<GameObject> objectsInScene;
    std::vector<Light> lightsInScene;

    public:
        Scene();
        ~Scene();

};
