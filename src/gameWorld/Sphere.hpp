#pragma once



#include <vector>
#include <GL/glew.h>
#include "glm/glm.hpp"
#include "commonAttributes.hpp"


namespace gWorld {
    /**
     * @brief Represents a discretised sphere centered in (0, 0, 0) (in its local coordinate system)
     * Its vertical axis is (0, 1, 0) and its horizontal axes are (1, 0, 0) and (0, 0, 1) 
     * 
     */
    class Sphere {
    /**
     * @brief Allocate and construct the data (implantation in the .cpp)
     * 
     * @param radius 
     * @param discLat 
     * @param discLong
     */
    void build(GLfloat radius, GLsizei discLat, GLsizei discLong);

public:
    /**
     * @brief Constructor: allocate the data array and construct the vertices' attributes
     * 
     */
    Sphere(GLfloat radius, GLsizei discLat, GLsizei discLong):
        m_nVertexCount(0) {
        build(radius, discLat, discLong); // Construction (see .cpp)
    }

    /**
     * @brief Get the Vertices Data object
     * 
     * @return const std::vector<ShapeVertex>  pointer to the data
     */
    const std::vector<ShapeVertex> getVerticesData() const {
        return m_Vertices;
    }
    
    /**
     * @brief Get the Vertex Count object
     * 
     * @return GLsizei number of vertices
     */
    GLsizei getVertexCount() const {
        return m_nVertexCount;
    }

private:
    std::vector<ShapeVertex> m_Vertices;
    GLsizei m_nVertexCount; // Number of vertices
};
}
