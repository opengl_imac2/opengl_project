//#include <cmath>
//#include <vector>
#include <iostream> // max()
#include <algorithm> // max()
#include "TrackballCamera.hpp"

namespace gWorld {

    TrackballCamera::TrackballCamera(){
        _fDistance = 5.0f;
        _fAngleX = 0.0f;
        _fAngleY = 0.0f;
        _target = glm::vec3(0.0,0.0,0.0);
    }

    void TrackballCamera::moveFront(float delta) {
        _fDistance = std::max(_fDistance+delta, 2.f); // so that the user doesn't zoom too far in and go through the object
    };

    void TrackballCamera::rotateLeft(float degrees) {
        _fAngleX -= degrees;
    };

    void TrackballCamera::rotateUp(float degrees) {
        _fAngleY -= degrees;
    };
    
    void TrackballCamera::setTarget(glm::vec3 updatedTarget){
        _target = updatedTarget;
    }

    glm::mat4 TrackballCamera::getViewMatrix() const noexcept {
        glm::mat4 VM = glm::translate(glm::mat4(1.0f), glm::vec3(0,0,-_fDistance));
        //VM = glm::rotate(VM,float(M_PI), glm::vec3(0,0,1));
        VM = glm::rotate(VM,_fAngleX, glm::vec3(1,0,0));
        VM = glm::rotate(VM,_fAngleY, glm::vec3(0,1,0));
        VM = glm::translate(VM, -_target);
        return VM;
    };
    
    
    
};