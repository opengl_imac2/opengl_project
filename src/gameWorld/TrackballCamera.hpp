#pragma once

#include <GL/glew.h>
#include "../openGL/Shader.hpp"
#include <glm/glm.hpp>
#include "commonAttributes.hpp"

namespace gWorld {
    /**
     * @brief Third Person Camera
     * 
     */
    class TrackballCamera {

        private :
       
        glm::vec3 _target;
        float _fDistance;
        float _fAngleX;
        float _fAngleY;

        public :
        /**
         * @brief Construct a new Trackball Camera object
         * 
         */
        TrackballCamera();

        /**
         * @brief Destroy the Trackball Camera object
         * 
         */
        ~TrackballCamera() noexcept = default;

        inline const float fAngleY() { return _fAngleY; }

        /**
         * @brief Zoom in the target
         * 
         * @param delta 
         */
        void moveFront(float delta);

        /**
         * @brief Rotate around the target horizontally
         * 
         * @param degrees 
         */
        void rotateLeft(float degrees);

        /**
         * @brief Rotate around the target vertically
         * 
         * @param degrees 
         */
        void rotateUp(float degrees);
        
        void setTarget(glm::vec3 updatedTarget);

        /**
         * @brief return the view matrix of the camera
         * 
         * @return glm::mat4 
         */
        glm::mat4 getViewMatrix() const noexcept;
        //glm::mat4 projectionMatrix(int width, int height);
    };
};