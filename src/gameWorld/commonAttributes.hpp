#pragma once

/**
 * @file commonAttributes.hpp
 * @brief Is linked to all our elements (sphere)
 * 
 */

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtx/io.hpp>
#include <glm/gtc/random.hpp>
#include <GL/glew.h>
#include "glm.hpp"

namespace gWorld{
    /**
     * @brief Vertices' structure
     * 
     */
    struct ShapeVertex {
        glm::vec3 position;
        glm::vec3 normal;
        glm::vec2 texCoords;
        
        
    };
}
