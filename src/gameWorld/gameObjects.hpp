// #pragma once

// /**
//  * @brief File defining all game objects, except main character
//  * 
//  */

// #include "../models/models.hpp"
// #include "../openGL/Shader.hpp"
// #include <string.h>

// /**
//  * @brief Abstract game object class
//  * 
//  */
// class gameObject {
//     public :
//         virtual gameObject() = 0;
//         virtual ~gameObject();
//         std::string getId();
//         bool getVisibility(int vis);

//     private :
//         std::string _id;
//         Model _model;
//         glm::vec3 _position; 
//         glm::vec3 _rotation;
//         glm::vec3 _scale;
//         bool visibilityDim1; // Visible in dimension 1
//         bool visibilityDim2; // Visible in dimension 2
// };

// /**
//  * @brief Decoration object that does not have any collider or interaction.
//  * 
//  */
// class landscapeObject : public gameObject {
//     public :
//         // Constructor
//         landscapeObject() = default;
//         /**
//          * @brief Construct a new landscape Object object
//          * 
//          * @param id Name identifying the object
//          * @param model Model object
//          * @param position 
//          * @param rotation 
//          * @param scale 
//          */
//         landscapeObject(
//             std::string id,
//             Model model,
//             glm::vec3 position,
//             glm::vec3 rotation,
//             glm::vec3 scale
//         );
//         // Destructor
//         ~landscapeObject();
    
//     private :
//         // Landscape object are visible in every dimension
//         bool visibilityDim1 = true;
//         bool visibilityDim2 = true;
// };

// /**
//  * @brief Decoration object that has a collider but no interaction.
//  * 
//  */
// class staticObject : public gameObject {
//     public :
//         // Constructor
//         staticObject() = default;
//         /**
//          * @brief Construct a new static Object object
//          * 
//          * @param id 
//          * @param model 
//          * @param position 
//          * @param rotation 
//          * @param scale 
//          * @param dim1 Is visible in dimension 1 ?
//          * @param dim2 Is visible in dimension 2 ?
//          */
//         staticObject(
//             std::string id,
//             Model model,
//             glm::vec3 position,
//             glm::vec3 rotation,
//             glm::vec3 scale,
//             bool dim1,
//             bool dim2
//         );
//         // Destructor
//         ~landscapeObject();
    
//     private :
//         // Landscape object are visible in every dimension
//         bool visibilityDim1 = true;
//         bool visibilityDim2 = true;
// };

// // WORK IN PROGRESS /////////////////////
// /**
//  * @brief Game object that has a collider and is interactive (calls a specific function on click).
//  * 
//  */
// class interactiveObject : public interactiveObject {
//     public :
//         // Constructor
//         interactiveObject() = default;
        
//         interactiveObject(
//             std::string id,
//             Model model,
//             glm::vec3 position,
//             glm::vec3 rotation,
//             glm::vec3 scale,
//             bool dim1,
//             bool dim2
//         );
//         // Destructor
//         ~landscapeObject();
    
//     private :
//         // Landscape object are visible in every dimension
//         bool visibilityDim1 = true;
//         bool visibilityDim2 = true;
// };
// // ^ WORK IN PROGRESS ^ ////////////////