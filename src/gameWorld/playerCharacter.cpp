#include "playerCharacter.hpp"

namespace gWorld{
    playerCharacter::playerCharacter()
        : _position(glm::vec3(0.f)), _model(Model("assets/models/MONKEY/monkey.gltf")), _box(1,1,1)
    {}

    playerCharacter::~playerCharacter(){
    }

    void playerCharacter::draw(openGL::Shader &shader) {
        _model.Draw(shader);
    }

    void playerCharacter::move(const Movement direction, const glm::mat4& viewMatrix) {
        // -->change of base
        float y = _position.y;
        glm::vec4 transformedPos = glm::vec4(_position, 1);
        glm::vec4 transformedDir = glm::vec4(_blockedDirection, 0); // using the same process to prevent moving if there's an obstacle
        // -->into camera coordinates
        transformedPos = viewMatrix*transformedPos;
        transformedDir = viewMatrix*transformedDir;
        // -->going in the given direction
            switch(direction) {
                case FRONT:
                    transformedPos.z -= 0.1;
                    break;
                case BACK:
                    transformedPos.z += 0.1;
                    break;
                case LEFT:
                    transformedPos.x -= 0.1;
                    break;
                case RIGHT:
                    transformedPos.x += 0.1;
                    break;
            }
            if (_blocked)
                transformedPos -= transformedDir*0.1f; // a bit dirty but it works
        // -->back to the ghost's coordinates
        _position = glm::vec3(glm::inverse(viewMatrix)*transformedPos);
        // -->y has to be conserved so that the ghost stays on the ground
        _position.y = y;
        _box.setCoordinates(_position.x, _position.y, _position.z);
    }
}
    