#pragma once

#include "../models/models.hpp"
#include "../openGL/Shader.hpp"
#include "Box.hpp" // Collision box


namespace gWorld {

    enum Movement {
        FRONT, BACK, LEFT, RIGHT
    };


    /**
     * @brief All the informations to move, draw and interact with the main character
     * 
     */
   class playerCharacter {
    private:
        glm::vec3 _position;
        Model _model;
        Box _box;
        bool _blocked = false;
        glm::vec3 _blockedDirection;

    public:
        /**
         * @brief Construct a new player Character object
         * 
         */
        playerCharacter();

        /**
         * @brief Destroy the player Character object
         * 
         */
        ~playerCharacter();

        /**
         * @brief Displays the character
         * 
         * @param shader 
         */
        void draw(openGL::Shader &shader);

        /**
         * @brief Move the character
         * 
         * @param mouvement Direction of the movement (FRONT, BACK, LEFT, RIGHT)
         * @param viewMatrix 
         */
        void move(const Movement mouvement, const glm::mat4& viewMatrix);

        /**
         * @brief Get the position
         * 
         * @return glm::vec3 
         */
        inline glm::vec3 getPos() { return _position; };

        /**
         * @brief Set if an object is blocking the character in any direction
         * 
         * @param blocked true if there is one, false if not
         */
        inline void setBlocked(const bool blocked) { _blocked = blocked; }

        /**
         * @brief Set the Blocked Direction object in the  world coordinates
         * 
         * @param side (0,0,1) if in the back, (0,0,-1) if on the front, (1,0,0) if on the right, (-1,0,0) if on the left
         */
        inline void setBlockedDirection(const glm::vec3 side) { _blockedDirection = side; }

        /**
         * @brief Get the collision box surrounding the main character
         * 
         * @return Box 
         */
        inline Box box() { return _box; }
    };

}
