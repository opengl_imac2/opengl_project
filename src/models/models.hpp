#pragma once

#include "../openGL/Shader.hpp"

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <assimp/pbrmaterial.h>

#include <glm/glm.hpp>

#include <vector>
#include <string>

/**
 * @brief Vertex of a mesh
 * 
 */
struct Vertex {
    glm::vec3 Position;
    glm::vec2 TexCoords;
    glm::vec3 Normal;
};

/**
 * @brief Texture
 * 
 */
struct Texture {
    unsigned int id;
    std::string type;
    std::string path;
};

/**
 * @brief Mesh object
 * 
 */
class Mesh {
    public :
        // Mesh data
        std::vector<Vertex> _vertices;
        std::vector<unsigned int> _indices;
        std::vector<Texture> _textures;

        /**
         * @brief Construct a new Mesh object
         * 
         * @param vertices Vector containing all vertices of the model
         * @param indices Vector containing all indices of the model
         * @param textures Vector containing all textures of the model
         */
        // Constructor
        Mesh (std::vector<Vertex> vertices, std::vector<unsigned int> indices, std::vector<Texture> textures);

        /**
         * @brief 
         * 
         * @param shader 
         */
        void Draw (openGL::Shader &shader);

    private :
        // Render data
        unsigned int VAO, VBO, EBO;
        
        /**
         * @brief Initializes Mesh's VAO with VBO and VEO, and enables 3 vertex attribs : position, normal and texture coord.
         * 
         */
        void setupMesh();
};

/**
 * @brief 3D Model composed of multiple Meshes
*/
class Model {
    public : 
        Model(char *path) {
            loadModel(path);
        }
        Model() {
        }
        /**
         * @brief Draw Model with specified shader
         * 
         * @param shader 
         */
    void Draw(openGL::Shader &shader);

    private:
        std::vector<Mesh> meshes; // Vector of meshes constituting the Model
        std::string directory; // File location
        
        /**
         * @brief Load model from file
         * 
         * @param path Path to model file
         */
        void loadModel(std::string path);

        /**
         * @brief Extract meshes from node and put them into the scene's meshes vector. Function is recursively called for every children node.
         * 
         * @param node First node of the mesh
         * @param scene Scne into wich all Models and Meshes are
         */
        void processNode(aiNode *node, const aiScene *scene);
       
        /**
         * @brief Create Mesh from aiMesh
         * 
         * @param mesh aiMesh generated in <void processNode(aiNode *node, const aiScene *scene)>
         * @param scene 
         * @return Mesh 
         */
        Mesh processMesh(aiMesh *mesh, const aiScene *scene);

        /**
         * @brief Iterates over all the textures of the given type, get 
         * every texture's file location than loads and generates a new
         * Texture object with it's information
         * 
         * @param mat 
         * @param type 
         * @param typeName 
         * @return std::vector<Texture> 
         */
        std::vector<Texture> loadMaterialTextures(aiMaterial *mat, aiTextureType type, std::string typeName);
};