#include "GBuffer.hpp"
#include <iostream> 

namespace openGL{

    GBuffer::GBuffer(unsigned int windowWidth, unsigned int windowHeight){
        _width = windowWidth;
        _height = windowHeight;
        GLCall(glGenFramebuffers(1, &_bufferID));
        init();
    };

    GBuffer::~GBuffer(){
        GLCall(glDeleteBuffers(1,&_bufferID));
    };

    void GBuffer::bindWriting() const{
        GLCall(glBindFramebuffer(GL_FRAMEBUFFER, _bufferID));
    }

    void GBuffer::bindReading() const{
        glBindFramebuffer(GL_READ_FRAMEBUFFER, _bufferID);
    }

    void GBuffer::bindTextures() const{
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, _gPosition);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, _gNormalMetallic);
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, _gDiffuseRoughness);
    }

    void GBuffer::unbind() const{
        GLCall(glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0));
    }

    bool GBuffer::init()
    {
        // position color buffer
        GLCall(glBindFramebuffer(GL_DRAW_FRAMEBUFFER, _bufferID));
        GLCall(glGenTextures(1, &_gPosition));
        GLCall(glBindTexture(GL_TEXTURE_2D, _gPosition));
        GLCall(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, _width, _height, 0, GL_RGBA, GL_FLOAT, NULL));
        GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST));
        GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST));
        GLCall(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, _gPosition, 0));
        // normal color buffer
        GLCall(glGenTextures(1, &_gNormalMetallic));
        GLCall(glBindTexture(GL_TEXTURE_2D, _gNormalMetallic));
        GLCall(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, _width, _height, 0, GL_RGBA, GL_FLOAT, NULL));
        GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST));
        GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST));
        GLCall(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, _gNormalMetallic, 0));
        // color + specular color buffer
        GLCall(glGenTextures(1, &_gDiffuseRoughness));
        GLCall(glBindTexture(GL_TEXTURE_2D, _gDiffuseRoughness));
        GLCall(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, _width, _height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL));
        GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST));
        GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST));
        GLCall(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, _gDiffuseRoughness, 0));

        // depth
        GLCall(glGenRenderbuffers(1, &_depthBuffer));
        GLCall(glBindRenderbuffer(GL_RENDERBUFFER, _depthBuffer));
        GLCall(glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, _width, _height));
        GLCall(glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, _depthBuffer));

        GLenum DrawBuffers[3] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2 };
        GLCall(glDrawBuffers(3, DrawBuffers));

        GLenum Status = glCheckFramebufferStatus(GL_FRAMEBUFFER);

        if (Status != GL_FRAMEBUFFER_COMPLETE) {
            printf("FB error, status: 0x%x\n", Status);
            return false;
        }

        // restore default FBO
        GLCall(glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0));

        return true;
    }

    void GBuffer::saveDepthBuffer() const{
        bindReading();
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
        glBlitFramebuffer(0, 0, _width, _height, 0, 0, _width, _height, GL_DEPTH_BUFFER_BIT, GL_NEAREST);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }
}
