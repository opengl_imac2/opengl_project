#pragma once
#include <GL/glew.h>
#include "../GLCall.h"
#include "../ImgTexture.hpp"
#include <vector>

namespace openGL{
    class GBuffer
    {
    public:

        GBuffer(unsigned int windowWidth, unsigned int windowHeight);

        ~GBuffer();

        bool init();

        void bindWriting() const;

        void bindReading() const;

        void bindTextures() const;
        
        void unbind() const;

        void saveDepthBuffer() const;

    private:

        GLuint _bufferID;
        GLuint _gPosition;
        GLuint _gNormalMetallic;
        GLuint _gDiffuseRoughness;
        GLuint _depthBuffer;

        unsigned int _width;
        unsigned int _height; 
    }; 
}
