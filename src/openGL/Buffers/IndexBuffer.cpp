#include "IndexBuffer.hpp"

using namespace openGL;

IndexBuffer::IndexBuffer() {
    GLCall(glGenBuffers(1, &_bufferID));
}

IndexBuffer::~IndexBuffer()  {
    GLCall(glDeleteBuffers(1, &_bufferID));
}

void IndexBuffer::bind(){
    GLCall(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _bufferID));
}

void IndexBuffer::unbind(){
    GLCall(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0));
}

void IndexBuffer::setData(const std::vector<glm::uvec3>& indices, const GLenum &drawType){
    bind();
     GLCall(glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(glm::uvec3) * indices.size(), indices.data(),  drawType));
    unbind();
}

//Maybe usefull

// void IndexBuffer::setVertexAttrib(const GLuint &VertexBufferID, const GLuint &size, GLenum dataType, const GLuint &stride, const GLuint &offset) {
//     GLCall(glEnableVertexAttribArray(VertexBufferID));
// 	GLCall(glVertexAttribPointer(VertexBufferID,size,dataType,GL_FALSE,stride,(const GLvoid*)(offset)));
// }

// void IndexBuffer::setVertexAttribInteger(const GLuint &VertexBufferID, const GLuint &size, GLenum dataType, const GLuint &stride, const GLuint &offset) {
//     GLCall(glEnableVertexAttribArray(VertexBufferID));
// 	GLCall(glVertexAttribIPointer(VertexBufferID,size,dataType,stride,(const GLvoid*)(offset)));
// }


// void IndexBuffer::setAttribDivisor(const GLuint &index, const GLubyte &divisor) {
//     GLCall(glVertexAttribDivisor(index, divisor));
// }
