#pragma once

#include <GL/glew.h>
#include <glm/glm.hpp>
#include "../GLCall.h"
#include <vector>

namespace openGL{

    class IndexBuffer
    {

        private:
            //---------- Attributes ----------//
            GLuint _bufferID;
            //GLuint _countID;

        public:
            IndexBuffer();
            ~IndexBuffer();
            //---------- Getters/Setters Methods ----------//
            
            inline GLuint getId() const { return _bufferID;}

            //---------- Other Methods ----------//
            
            void bind();
            void unbind();

            

            ///
            ///set data to VertexBuffer (GL_ARRAY_BUFFER)
            ///indices data as a template vector 
            ///drawType GLenum specifies the expected usage of the data (GL_STATIC_DRAW, GL_STREAM_DRAW, GL_DYNAMIC_DRAW, ... )
            ///
            void setData(const std::vector<glm::uvec3>& indices, const GLenum &drawType);
    };

}
