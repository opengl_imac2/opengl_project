#include "VertexArray.hpp"
#include <iostream>

namespace openGL{
    VertexArray::VertexArray(){
        GLCall(glGenVertexArrays(1, &_bufferID)); 
    }

    VertexArray::~VertexArray(){
        GLCall(glDeleteVertexArrays(1, &_bufferID));
    }

    void VertexArray::bind() const{
        GLCall(glBindVertexArray(_bufferID));
    }

    void VertexArray::unbind() const{
        GLCall(glBindVertexArray(0));
    }

    void VertexArray::addBuffer(const VertexBuffer& vb, const VertexBufferLayout& layout){
        bind();
        vb.bind();
        const auto& elements = layout.getElements(); //we 
        unsigned int offset = 0;
        for (unsigned int i = 0; i < elements.size(); i++){
            const auto& element = elements[i];
            GLCall(glEnableVertexAttribArray(i));
            // GLCall(glVertexAttribPointer(i,element._count, element._type, element._normalized, layout.getStride(), (GLvoid *))offset));
            GLCall(glVertexAttribPointer(i,element._count, element._type, element._normalized, layout.getStride(), static_cast<const char*>(0)+offset));
            offset +=element._count * sizeof(element._type);
            

        }
        vb.unbind();
    }
}



