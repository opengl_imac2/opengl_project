#pragma once


#include <GL/glew.h>
#include <glm/glm.hpp>
#include "VertexBuffer.hpp"
#include "VertexBufferLayout.hpp"
#include "../GLCall.h"
#include <vector>

namespace openGL
{
    /**
     * @brief Create VAO
     * 
     */
    class VertexArray
    {
        private :
        GLuint _bufferID;


        public :
        /**
         * @brief Construct a new Vertex Array object
         * 
         */
        VertexArray();
        /**
         * @brief Destroy the Vertex Array object
         * 
         */
        ~VertexArray();
        /**
         * @brief bind the VAO
         * 
         */
        void bind() const;
        /**
         * @brief unbind the VAO
         * 
         */
        void unbind() const;

        /**
         * @brief Import the VBO into the VAO and enable the attributes
         * 
         * @param vb 
         * @param layout 
         */
        void addBuffer(const VertexBuffer& vb, const VertexBufferLayout& layout);
    } ;
} // namespace openGL

