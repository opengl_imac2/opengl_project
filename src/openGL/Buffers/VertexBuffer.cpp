#include "VertexBuffer.hpp"

using namespace openGL;

VertexBuffer::VertexBuffer() {
    GLCall(glGenBuffers(1, &_bufferID));
}

VertexBuffer::~VertexBuffer() {
    GLCall(glDeleteBuffers(1, &_bufferID));
}

void VertexBuffer::bind() const{
    GLCall(glBindBuffer(GL_ARRAY_BUFFER, _bufferID));
}

void VertexBuffer::unbind() const{
    GLCall(glBindBuffer(GL_ARRAY_BUFFER, 0));
}

//Maybe usefull

// void VertexBuffer::setVertexAttrib(const GLuint &VertexBufferID, const GLuint &size, GLenum dataType, const GLuint &stride, const GLuint &offset) {
//     GLCall(glEnableVertexAttribArray(VertexBufferID));
// 	GLCall(glVertexAttribPointer(VertexBufferID,size,dataType,GL_FALSE,stride,(const GLvoid*)(offset)));
// }

// void VertexBuffer::setVertexAttribInteger(const GLuint &VertexBufferID, const GLuint &size, GLenum dataType, const GLuint &stride, const GLuint &offset) {
//     GLCall(glEnableVertexAttribArray(VertexBufferID));
// 	GLCall(glVertexAttribIPointer(VertexBufferID,size,dataType,stride,(const GLvoid*)(offset)));
// }


// void VertexBuffer::setAttribDivisor(const GLuint &index, const GLubyte &divisor) {
//     GLCall(glVertexAttribDivisor(index, divisor));
// }
