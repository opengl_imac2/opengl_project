#pragma once



#include <GL/glew.h>
#include <glm/glm.hpp>
#include "../GLCall.h"
#include <vector>

namespace openGL
{
    /**
     * @brief Generate the vbo
     * 
     */
    class VertexBuffer
{

    private:
        //---------- Attributes ----------//
        GLuint _bufferID;

    public:
        /**
         * @brief Construct a new Vertex Buffer object
         * 
         */
        VertexBuffer();
        /**
         * @brief Destroy the Vertex Buffer object
         * 
         */
        ~VertexBuffer();

         //---------- Getters/Setters Methods ----------//
        /**
         * @brief Get the Id object
         * 
         * @return GLuint 
         */
		inline GLuint getId() const { return _bufferID;}

        //---------- Other Methods ----------//
        /**
         * @brief Bind the VBO
         * 
         */
        void bind() const;
        /**
         * @brief Unbind the VBO
         * 
         */
        void unbind() const;

        
        // static void setVertexAttrib(const GLuint &VertexBufferID, const GLuint &size, GLenum dataType, const GLuint &stride, const GLuint &offset);
		// static void setVertexAttribInteger(const GLuint &VertexBufferID,const GLuint &size, GLenum dataType,const GLuint &stride,const GLuint &offset);
		// static void setAttribDivisor(const GLuint &index, const GLubyte &divisor);
        
        /**
         * @brief Set the Data object to VertexBuffer (GL_ARRAY_BUFFER)
         * indices data as a template vector
         *  
         * @tparam T 
         * @param data 
         * @param drawType specifies the expected usage of the data (GL_STATIC_DRAW, GL_STREAM_DRAW, GL_DYNAMIC_DRAW, ... )
         */
        template<typename T>
        void setData(const std::vector<T>& data, const GLenum& drawType) 
        {
            bind();
                GLCall(glBufferData(GL_ARRAY_BUFFER, sizeof(T) * data.size(), data.data(), drawType));
            unbind();
        }
};
} // namespace openGL

