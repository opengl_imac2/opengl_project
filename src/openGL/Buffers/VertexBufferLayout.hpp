#pragma once

#include <vector>
#include <GL/glew.h>
#include <iostream>

namespace openGL
{
    /**
     * @brief Attribute's informations
     * 
     */
    struct vbElements
    {
        unsigned int _type;
        unsigned int _count;
        unsigned char _normalized;
    };

    /**
     * @brief  Import the attribute's informations
     */
    class VertexBufferLayout
    {
    private:
        std::vector<vbElements> _elements;
        /**
         * @brief Size of the element in bit
         * 
         */
        unsigned int _stride = 0;
    public:
        /**
         * @brief Construct a new Vertex Buffer Layout object
         * 
         */
        VertexBufferLayout(){};
        /**
         * @brief Destroy the Vertex Buffer Layout object
         * 
         */
        ~VertexBufferLayout(){};
        /**
         * @brief setup all the vbElements for each elements
         * 
         * @param elements 
         */
        void setup(std::initializer_list<vbElements> const & elements) {
            for (vbElements element : elements) {
                _elements.push_back(element);
                _stride += sizeof(element._type) * element._count ;
                // std::cout << _stride << std::endl;
            }
        }

        // -------------------------------------  Getter/Setter ------------------------ //
        /**
         * @brief Get the Elements object
         * 
         * @return const std::vector<vbElements> 
         */
        inline const std::vector<vbElements> getElements() const { return _elements; } 
        /**
         * @brief Get the Stride object
         * 
         * @return unsigned int 
         */
        inline unsigned int getStride() const { return _stride; } 
    };
}


