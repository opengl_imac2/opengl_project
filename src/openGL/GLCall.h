#pragma once

#include <GL/glew.h>
#include <assert.h>
#define ARRAY_SIZE_IN_ELEMENTS(a) (sizeof(a)/sizeof(a[0]))
// Opengl functions debug and display with assertion

#ifndef NDEBUG
    #define GLCall(x) glDebug::clearErrors(); x; assert(!glDebug::checkErrors(#x, __FILE__, __LINE__))
#else
    #define GLCall(x) x
#endif

/**
 * @brief OpenGL functions debug and display with assertion
 * 
 */
namespace glDebug {

    void clearErrors();
    bool checkErrors(const char* functionName, const char* filename, int line);
    char const* errorString(GLenum const err);

};