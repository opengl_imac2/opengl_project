#include "ImgTexture.hpp"
#include <stb_image/stb_image.h>

namespace openGL{
    ImgTexture::ImgTexture(const std::string& path, TextType _type)
    : _texID(0), _filePath(path), _localBuffer(nullptr),
    _width(0), _height(0), _BPP(0)
    {
        _localBuffer = stbi_load(path.c_str(), &_width, &_height, &_BPP, 4);
        GLCall(glGenTextures(1,&_texID));
        GLCall(glBindTexture(GL_TEXTURE_2D, _texID));

        GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR));
        GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR));
        GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
        GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));

        GLCall(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, _width, _height, 0, GL_RGBA, GL_UNSIGNED_BYTE, _localBuffer));
        GLCall(glBindTexture(GL_TEXTURE_2D, 0));
    }

    ImgTexture::~ImgTexture(){
        GLCall(glDeleteTextures(1, &_texID));
    }

    void ImgTexture::bind(unsigned int slot) const{
        GLCall(glActiveTexture(GL_TEXTURE0 + slot));
        GLCall(glBindTexture(GL_TEXTURE_2D, _texID));
    }

    void ImgTexture::unbind() const{
        GLCall(glBindTexture(GL_TEXTURE_2D, 0));
    }
}
