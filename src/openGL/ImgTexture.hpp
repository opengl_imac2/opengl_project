#pragma once

#include "GLCall.h"
#include <iostream>

namespace openGL{
    /**
     * @brief Enumeration of possible Texture types
     * 
     */
    enum TextType{
        DIFFUSE = 0, SPECULAR, NORMAL, ROUGHNESS, HEIGHTMAP, DENSITY
    };
    /**
     * @brief To load an image as texture and bind it
     * 
     */
    class ImgTexture
    {
    private:
        unsigned int _texID;
        std::string _filePath;
        unsigned char* _localBuffer;
        int _width,_height, _BPP;
        TextType _type;


    public:
        /**
         * @brief Construct a new ImgTexture object
         * 
         */
        ImgTexture(const std::string& path, TextType _type = DIFFUSE);
        /**
         * @brief Destroy the ImgTexture object
         * 
         */
        ~ImgTexture();
        /**
         * @brief bind a texture from a precised slot (setted up on 0 by default)
         * 
         * @param slot 
         */
        void bind(unsigned int slot = 0) const;
        /**
         * @brief unbind a texture
         * 
         */
        void unbind() const;
        
        inline unsigned int getTexID(){return _texID;};
        
    };
}


