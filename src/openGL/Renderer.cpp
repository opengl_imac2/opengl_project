#include "Renderer.hpp"
#include <iostream>

namespace openGL{

    Renderer::Renderer(float windowWidth, float windowHeight, gWorld::playerCharacter* chara)
        : _controller(), _gBuffer(windowWidth,windowHeight), _projMatrix(glm::perspective(glm::radians(70.f), windowWidth/windowHeight, 0.1f,100.0f)),_charaController(chara, _controller.camera())
    {}
    
    Renderer::~Renderer(){};

    void Renderer::draw(const VertexArray& va, const GLsizei& vertexCount, const Shader& shader) const{
        shader.use();
        va.bind();
            glDrawArrays(GL_TRIANGLES, 0, vertexCount);
        va.unbind();
    } ;

    void Renderer::clear() const{
        
        GLCall(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
    }

    void Renderer::geometryPass(Shader &shader){
        GLCall(glDisable(GL_BLEND));
        glClearColor(_clearColor[0], _clearColor[1], _clearColor[2], _clearColor[3]);
        clear();

        _gBuffer.bindWriting();
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        clear();

        shader.use();
        //glm::mat4 MVMatrix = glm::translate(renderer.getRenderViewMatrix(), glm::vec3(0.f,0.f,0.0f));
        shader.setMat4("uProjMatrix", _projMatrix);
        shader.setMat4("uViewMatrix", getRenderViewMatrix());
        
        //Complete with scene drawings

    }
    void Renderer::lightingPass(Shader &shader, glm::vec3 viewPos, std::vector<glm::vec3> &lightPositions, std::vector<glm::vec3> &lightColors){
        glClearColor(_clearColor[0], _clearColor[1], _clearColor[2], _clearColor[3]);
        clear();

        shader.use();
        shader.setVec3f("uViewPos", viewPos);
        _gBuffer.bindTextures();
        for (unsigned int i = 0; i < lightPositions.size(); i++)
        {
            
            shader.setVec3f("lightPositions[" + std::to_string(i) + "]", lightPositions[i]);
            shader.setVec3f("lightColors[" + std::to_string(i) + "]", lightColors[i]);
        }
        renderFrame();

        _gBuffer.saveDepthBuffer();
        glEnable(GL_BLEND);
        //Complete with scene drawings

    }

    void Renderer::renderFrame() {
        if (_frameVAO == 0){    
            float frameVertices[] = {
                // positions        // texture Coords
                -1.0f,  1.0f, 0.0f, 0.0f, 1.0f,
                -1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
                1.0f,  1.0f, 0.0f, 1.0f, 1.0f,
                1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
            };
            // setup plane VAO
            GLCall(glGenVertexArrays(1, &_frameVAO));
            GLCall(glGenBuffers(1, &_frameVBO));
            GLCall(glBindVertexArray(_frameVAO));
            GLCall(glBindBuffer(GL_ARRAY_BUFFER, _frameVBO));
            GLCall(glBufferData(GL_ARRAY_BUFFER, sizeof(frameVertices), &frameVertices, GL_STATIC_DRAW));
            GLCall(glEnableVertexAttribArray(0));
            GLCall(glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0));
            GLCall(glEnableVertexAttribArray(1));
            GLCall(glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float))));
        }
        GLCall(glBindVertexArray(_frameVAO));
        GLCall(glDrawArrays(GL_TRIANGLE_STRIP, 0, 4));
        GLCall(glBindVertexArray(0));
    }

    void Renderer::renderCube(){
        // initialize (if necessary)
        if (_cubeVAO == 0)
        {
            float vertices[] = {
                // back face
                -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 0.0f, // bottom-left
                1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 1.0f, // top-right
                1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 0.0f, // bottom-right         
                1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 1.0f, // top-right
                -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 0.0f, // bottom-left
                -1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 1.0f, // top-left
                // front face
                -1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f, // bottom-left
                1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 0.0f, // bottom-right
                1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 1.0f, // top-right
                1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 1.0f, // top-right
                -1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 1.0f, // top-left
                -1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f, // bottom-left
                // left face
                -1.0f,  1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-right
                -1.0f,  1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 1.0f, // top-left
                -1.0f, -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-left
                -1.0f, -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-left
                -1.0f, -1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 0.0f, // bottom-right
                -1.0f,  1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-right
                // right face
                1.0f,  1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-left
                1.0f, -1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-right
                1.0f,  1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 1.0f, // top-right         
                1.0f, -1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-right
                1.0f,  1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-left
                1.0f, -1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 0.0f, // bottom-left     
                // bottom face
                -1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 1.0f, // top-right
                1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 1.0f, // top-left
                1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f, // bottom-left
                1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f, // bottom-left
                -1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 0.0f, // bottom-right
                -1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 1.0f, // top-right
                // top face
                -1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 1.0f, // top-left
                1.0f,  1.0f , 1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f, // bottom-right
                1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 1.0f, // top-right     
                1.0f,  1.0f,  1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f, // bottom-right
                -1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 1.0f, // top-left
                -1.0f,  1.0f,  1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 0.0f  // bottom-left        
            };
            glGenVertexArrays(1, &_cubeVAO);
            glGenBuffers(1, &_cubeVBO);
            // fill buffer
            glBindBuffer(GL_ARRAY_BUFFER, _cubeVBO);
            glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
            // link vertex attributes
            glBindVertexArray(_cubeVAO);
            glEnableVertexAttribArray(0);
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
            glEnableVertexAttribArray(1);
            glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
            glEnableVertexAttribArray(2);
            glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
            glBindBuffer(GL_ARRAY_BUFFER, 0);
            glBindVertexArray(0);
        }
        // render Cube
        glBindVertexArray(_cubeVAO);
        glDrawArrays(GL_TRIANGLES, 0, 36);
        glBindVertexArray(0);
    }

    void Renderer::updateCameraTarget(const glm::vec3 position){
        _controller.updateTarget(position);
    }

    void Renderer::cameraControl(const glimac::SDLWindowManager& windowManager, const SDL_Event& event) {
        _controller.onEvent(windowManager, event);
    }

    void Renderer::charaControl(const SDL_Event& event) {
        _charaController.onEvent(event);
    }

}