#pragma once

#include <GL/glew.h>
#include "GLCall.h"
#include "Buffers/VertexArray.hpp"
#include "Buffers/IndexBuffer.hpp"
#include "Buffers/GBuffer.hpp"
#include "Shader.hpp"
//#include "../gameWorld/TrackballCamera.hpp"
//#include "../SDLWindowManager/SDLWindowManager.hpp"
#include "../gameWorld/CameraController.hpp" //to manage the camera
#include "../gameWorld/CharaController.hpp" //to manage the main character


namespace openGL{
    /**
     * @brief Draw-Calls gestion / Deferred Rendering / ViewMatrix  
     * 
     */
    class Renderer
    {
        private :
        GLuint _frameVAO = 0;
        GLuint _frameVBO;
        GLuint _cubeVAO = 0;
        GLuint _cubeVBO = 0;
        GBuffer _gBuffer;
        gWorld::CameraController _controller;
        glm::vec4 _clearColor = {0.1f, 0.7f, 0.9f, 1.0f}; //daylight
        //glm::vec4 _clearColor = {0.3f, 0.05f, 0.45f, 1.0f}; //altered world
        //glm::vec4 _clearColor = {0.0f, 0.0f, 0.0f, 0.0f};
        glm::mat4 _projMatrix;
        gWorld::CharaController _charaController;


        public :
        /**
         * @brief Construct a new Renderer object
         * 
         */
        Renderer(float windowWidth, float windowHeight, gWorld::playerCharacter* chara);

        /**
         * @brief Destroy the Renderer object
         * 
         */
        ~Renderer();

        /**
         * @brief Very basic drawCall to render a VertexArray with a shader. Meant to be modified with deferred shading and materials.
         * 
         * @param va 
         * @param vertexCount 
         * @param shader 
         */
        void draw(const VertexArray& va, const GLsizei& vertexCount, const Shader& shader) const;

        /**
         * @brief basic GlClear in the render loop
         * 
         */
        void clear() const;

        /**
         * @brief Control camera with user input (maybe modified)
         * 
         * @param windowManager 
         * @param event 
         */
        void cameraControl(const glimac::SDLWindowManager& windowManager, const SDL_Event& event);

        /**
         * @brief Control the main character with user inputs
         * 
         * @param event 
         */
        void charaControl(const SDL_Event& event);

         /**
         * @brief updates camera position when the player moves
         * 
         * @param position 
         */
        void updateCameraTarget(const glm::vec3 position);

        /**
         * @brief Render a frame (for deferred rendering after post-processing)
         * 
         */
        void geometryPass(Shader &shader);

        void lightingPass(Shader &shader, glm::vec3 viewPos, std::vector<glm::vec3> &lightPositions, std::vector<glm::vec3> &lightColors);

        void renderFrame();
        /**
         * @brief Render a cube used to display light position and test combined deferred ans forward rendering
         * 
         */
        void renderCube();
        /**
         * @brief Get the Render View Matrix
         * 
         * @return glm::mat4 
         */
        inline glm::mat4 getRenderViewMatrix() { return _controller.getViewMatrix(); };

        inline GBuffer getGBuffer(){ return _gBuffer;};
    };
}
