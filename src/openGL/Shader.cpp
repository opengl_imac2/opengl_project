#include "Shader.hpp"
#include "GLCall.h"
#include <iostream>
#include <vector>
#include <sstream>
#include <fstream>

using namespace openGL;

//-----------------------------------Constructor without Geometry shader--------------------------------------//

Shader::Shader(const char* vertS, const char* fragS, const bool &fromFile) 
    : _shaderId(0), _vsId(0), _fsId(0), m_compiled(false) {

    std::string vsStrPath;
    std::string fsStrPath;

    if (fromFile) {
        vsStrPath = findFile(vertS);
        fsStrPath = findFile(fragS);
    }else {
        vsStrPath = vertS;
        fsStrPath = fragS;            
    }
    // std::cout << "_shaderId " << _shaderId << std::endl;
    // std::cout << "_vsId " << _vsId << std::endl;
    // std::cout << "_fsId " << _fsId << std::endl;
    // std::cout << "vsStrPath " << vsStrPath << std::endl;
    // std::cout << "fsStrPath " << fsStrPath << std::endl;

	GLCall(_shaderId = glCreateProgram()); // attribute program id 

    // compile shaders
    GLCall(_vsId = compileShader(GL_VERTEX_SHADER, vsStrPath));
	GLCall(_fsId = compileShader(GL_FRAGMENT_SHADER, fsStrPath));

    attachShaderId("vertex", _vsId);
    attachShaderId("fragment", _fsId);
 
    //link them 
	GLCall(glLinkProgram(_shaderId));

	int linkResult;
	GLCall(glGetProgramiv(_shaderId, GL_LINK_STATUS, &linkResult));

	if (linkResult == GL_FALSE) {
		GLCall(glDeleteProgram(_shaderId));
		std::cerr << "Shader creation failed (Impossible to link)" << std::endl;
    } else {
		GLCall(glValidateProgram(_shaderId));
		m_compiled = true;
    }

    // delete the shaders as they're linked into our program now and no longer necessery
    glDeleteShader(_vsId);
    glDeleteShader(_fsId);
}

Shader::~Shader() {
    if (_shaderId != 0)
        GLCall(glDeleteProgram(_shaderId));
}

void Shader::attachShaderId(const char* shaderName, const GLuint id) {
    if (id != 0) {
        GLCall(glAttachShader(_shaderId, id));
        std::cout << shaderName << " shader attached " << std::endl;
    }
    else {
        std::cout << "[Shader] creation aborded(" << shaderName << "shaders not compiled)" << std::endl;
        assert(false);
    }
}

GLuint Shader::compileShader(const GLenum &shaderType, const std::string &shaderStr) {

    GLuint shaderId = glCreateShader(shaderType);

    if(shaderId) {

        const char* shaderCode = shaderStr.c_str();
        // const GLint shaderCodeSize = shaderStr.size();
        GLCall(glShaderSource(shaderId, 1, &shaderCode, nullptr));
        GLCall(glCompileShader(shaderId));

        //Debug 
        int compileResult;
        GLCall(glGetShaderiv(shaderId, GL_COMPILE_STATUS, &compileResult));
        if (compileResult == GL_FALSE) {
            int infosLength;
            GLCall(glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &infosLength));
            std::vector<char> message(infosLength);
            GLCall(glGetShaderInfoLog(shaderId, infosLength, &infosLength, &message[0]));
            std::cout << "[Shader] error during compiling shader (" << shaderTypeStr(shaderType) << ") :" << std::endl << &message[0] << std::endl;
            return 0;
        }
    } else {
        std::cerr << "[Shader] Failed to assign new shader id (" << shaderTypeStr(shaderType) <<  " shader)" << std::endl;
    }
    return shaderId;
}

std::string Shader::findFile(const char* filepath) {

    std::string shaderStrPath;
    std::ifstream file(filepath, std::ios::in);

    if (!file.is_open()){
		std::cerr << "[Shader] Failed to open file : " + *filepath << std::endl;
    }

    // with string stream
    std::stringstream stream;
    stream << file.rdbuf();
    file.close();	
    shaderStrPath = stream.str();
    
    return shaderStrPath;
}

bool Shader::use() const{
	if (m_compiled) {
		GLCall(glUseProgram(_shaderId));
	}
    return m_compiled;
};

std::string Shader::shaderTypeStr(const GLenum &shaderType) {
	switch (shaderType) {
	case GL_FRAGMENT_SHADER:
		return "GL_FRAGMENT_SHADER";

	case GL_GEOMETRY_SHADER:
		return "GL_GEOMETRY_SHADER";

	case GL_COMPUTE_SHADER:
		return "GL_COMPUTE_SHADER";

	case GL_VERTEX_SHADER:
		return "GL_VERTEX_SHADER";

	case GL_TESS_CONTROL_SHADER:
		return "GL_TESS_CONTROL_SHADER";

	case GL_TESS_EVALUATION_SHADER:
		return "GL_TESS_EVALUATION_SHADER";

	default:
		assert(!"unknown shader type");
		return nullptr;
	}
}

//----------------------------------------------Uniform------------------------------------------------------//


GLint Shader::getUniform(const std::string &uniformName) {
    if (_uniformLocationCache.find(uniformName) != _uniformLocationCache.end()) {
        return _uniformLocationCache[uniformName];
    }

	GLCall(GLint location = glGetUniformLocation(_shaderId, uniformName.c_str()));

    if (location ==  -1) {
        std::cerr << "[Shader] uniform \"" << uniformName << "\" doesn't exist or is useless !" << std::endl;

    }

	_uniformLocationCache[uniformName] = location;
    return location;
}

void Shader::setInt(const std::string &uniformName, int v) {
    GLCall(glUniform1i(getUniform(uniformName), v));
}
void Shader::setFloat(const std::string &uniformName, float v) {
    GLCall(glUniform1f(getUniform(uniformName), v));
}
void Shader::setVec2f(const std::string &uniformName, const glm::vec2 &v) {
    GLCall(glUniform2f(getUniform(uniformName), v.x, v.y));
}
void Shader::setVec2f(const std::string &uniformName, const float &x, const float &y) {
    GLCall(glUniform2f(getUniform(uniformName), x, y));
}
void Shader::setVec3f(const std::string &uniformName, const glm::vec3 &v) {
    GLCall(glUniform3f(getUniform(uniformName), v.x, v.y, v.z));
}
void Shader::setVec3f(const std::string &uniformName, const float &x, const float &y, const float &z) {
    GLCall(glUniform3f(getUniform(uniformName), x, y, z));
}
void Shader::setVec4f(const std::string &uniformName, const glm::vec4 &v) {
    GLCall(glUniform4f(getUniform(uniformName), v.x, v.y, v.z, v.w));
}
void Shader::setVec4f(const std::string &uniformName, const float &x, const float &y, const float &z, const float &w) {
    GLCall(glUniform4f(getUniform(uniformName), x, y, z, w));
}
void Shader::setMat3(const std::string &uniformName, const glm::mat3 &m) {
    GLCall(glUniformMatrix3fv(getUniform(uniformName), 1, GL_FALSE, &m[0][0]));
}
void Shader::setMat4(const std::string &uniformName, const glm::mat4 &m) {
    GLCall(glUniformMatrix4fv(getUniform(uniformName), 1, GL_FALSE, &m[0][0]));
}



//-----------------------------------Constructor with Geometry shader--------------------------------------//


// Shader::Shader(const char* vertS, const char* fragS, const char* geomS, const bool &fromFile) 
//     : _shaderId(0), _vsId(0), _fsId(0), m_compiled(false) {

//     std::string vsStr;
//     std::string fsStr;
//     std::string gsStr;

//     if (fromFile) {
//         vsStr = findFile(vertS);
//         fsStr = findFile(fragS);
//         // if(geomS != nullptr) {
//         //     gsStr = parseFile(geomS);
//         // } else {
//         //     gsStr = "";
//         // }
//     }else {
//         vsStr = vertS;
//         fsStr = fragS;
//         // if(geomS != nullptr) {
//         //     gsStr = geomS;
//         // }else {
//         //     gsStr = "";
//         // }
            
//     }

// 	GLCall(_shaderId = glCreateProgram()); // attribute program id 

//     // compile shaders
//     GLCall(_vsId = compileShader(GL_VERTEX_SHADER, vsStr));
// 	GLCall(_fsId = compileShader(GL_FRAGMENT_SHADER, fsStr));
//     // if (geomS != nullptr) {
//     //     GLCall(m_gsID = compileShader(GL_GEOMETRY_SHADER, gsStr));
//     // }

//     attachShaderId("vertex", _vsId);
//     attachShaderId("fragment", _fsId);
//     // if (geomS != nullptr) {
//     //     attachShaderId("geometry", m_gsID);
//     // }
 
//     //link them 
// 	GLCall(glLinkProgram(_shaderId));

// 	int linkResult;
// 	GLCall(glGetProgramiv(_shaderId, GL_LINK_STATUS, &linkResult));
// 	if (linkResult == GL_FALSE) {
// 		GLCall(glDeleteProgram(_shaderId));
// 		std::cerr << "Shader creation aborded (linking doesn't work)" << std::endl;
// 		// _compiled = false; // already false
//     } else {
// 		GLCall(glValidateProgram(_shaderId));
// 		m_compiled = true;
//     }
//     // delete the shaders as they're linked into our program now and no longer necessery
//     glDeleteShader(_vsId);
//     glDeleteShader(_fsId);
//     // if (geomS != nullptr)
//     //     glDeleteShader(m_gsID);
// }