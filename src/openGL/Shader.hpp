#pragma once
/**
 * @file Shader.hpp
 * @brief Managing Shader's
 */

#include <string>
#include <unordered_map>
#include <GL/glew.h>
#include "glm/glm.hpp"

namespace openGL
{

    /**
     * @brief Shader class
     * 
     */
    class Shader
    {
        private:
            //-------------------------- Attributes -------------------------------//
            GLuint _shaderId;
            GLuint _vsId;
            GLuint _fsId;
            //GLuint m_gsId;
            std::unordered_map<std::string, GLint> _uniformLocationCache;

            bool m_compiled;


            //-------------------------- Methods -------------------------------//
            /**
             * @brief Try to compile shader. Send error messages if it is already compiled or if compiling fails.
             * 
             * @param shaderType Vertex shader || Fragment shader
             * @param shaderStr Content of the entire glsl file, from <findFile>"(const char *filePath)"
             * @return GLuint 
             */
            GLuint compileShader(const GLenum &shaderType, const std::string &shaderStr);

            /**
             * @brief Get the shader and send back it's content in a string
             * 
             * @param filePath Relative path to shader file
             * @return std::string 
             */
            std::string findFile(const char *filePath);

            /**
             * @brief Debug function returning a string of what type of shader failed, GL_Fragment_Shader or GL_Vertex_Shader
             * 
             * @param shaderType OpenGL shader type, GL_Fragment_Shader or GL_Vertx_Shader
             * @return std::string 
             */
            std::string shaderTypeStr(const GLenum &shaderType);

            /**
             * @brief Attach OpenGL shader's id to Shader class object
             * 
             * @param shaderName Char* indicating type of shader : "Vertex" or "Fragment"
             * @param id OpenGL shader's id
             */
            void attachShaderId(const char *shaderName, const GLuint id);

        public:
            Shader() = default;
            //Shader(const Shader& s); TODO

            /**
             * @brief Construct a new Shader object
             * 
             * @param vertS Path to vertex shader
             * @param fragS Path to fragment shader
             * @param fromFile Default = true (Recommended). False allows to directly pass shaders from char* 
             */
            Shader(const char *vertS, const char *fragS, const bool &fromFile = true);
            //Shader(const char* vertS, const char* fragS, const char* geomS = nullptr, const bool &fromFile = true); maybe usefull
            ~Shader();

            bool use() const;
            inline void stopUse() const { glUseProgram(-1); }

             //--------------------------- Getters/Setters Methods  -------------------//
            /**
             * @brief Getter Shader'ID
             * 
             * @return GLuint 
             */
            inline GLuint getID() const { return _shaderId; };
            
            //---------- Uniforms Methods ----------//

            /**
             * \defgroup Uniform variables setting
             * @brief Set uniform variables
             */
            GLint getUniform(const std::string &uniformName);
            void setInt(const std::string &uniformName, int v);
            void setFloat(const std::string &uniformName, float v);
            void setVec2f(const std::string &uniformName, const glm::vec2 &v);
            void setVec2f(const std::string &uniformName, const float &x, const float &y);
            void setVec3f(const std::string &uniformName, const glm::vec3 &v);
            void setVec3f(const std::string &uniformName, const float &x, const float &y, const float &z);
            void setVec4f(const std::string &uniformName, const glm::vec4 &v);
            void setVec4f(const std::string &uniformName, const float &a, const float &b, const float &c, const float &d);
            void setMat3(const std::string &uniformName, const glm::mat3 &mat);
            void setMat4(const std::string &uniformName, const glm::mat4 &mat);
            ///@{
        };

} // namespace openGL