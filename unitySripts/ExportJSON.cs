using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

public class ExportJSON : MonoBehaviour {
	
	string filename = "sceneData.json";
	string path;

	// Object that will stock all objects info
	jsonData data = new jsonData();
	
	// Called at the beginning
	void Start() {
		Debug.Log("JSON SAVING STARTED");
		path = Application.persistentDataPath + "/" + filename;
		setData();
		saveData();
	}

	// Set all objects data in fiels
	void setData() {

		// Get all objects of the scene
		GameObject[] sceneObjects = UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects();

		for (int i = 0; i < sceneObjects.Length; i++) {
			// Save current object in loop as o for simplicity
			GameObject o = sceneObjects[i];
			
			// CHECK IF OBJECT TYPE EXISTS
			if (o.GetComponent<ObjType>() == null) {
				Debug.Log("ObjType is not defined for object : " + o.name);

			} else {
				// PREPARING REGEX
				string currentObjectName = Regex.Match(o.name, @"^()\w+").Value;
				Debug.Log("currentObjectName : " + currentObjectName);

				// STATIC OBJECT
				if (o.GetComponent<ObjType>().type == "static") {
					if (data.staticObjList.Exists(ele => Regex.IsMatch(Regex.Match(ele.name, @"^()\w+").Value, currentObjectName))) {

						// Create new static obj
						staticObj newObj = new staticObj(
							o.name,
							o.transform.position.x,
							o.transform.position.y,
							o.transform.position.z,
							o.transform.rotation.x,
							o.transform.rotation.y,
							o.transform.rotation.z,
							o.transform.lossyScale.x,
							o.transform.lossyScale.y,
							o.transform.lossyScale.z
						);

						// Add object to existing instances
						data.staticObjList.Find(ele => Regex.IsMatch(Regex.Match(ele.name, @"^()\w+").Value, currentObjectName)).instances.Add(newObj);

						Debug.Log("Added new instance of : " + o.name);

					} else {

						// Create new static obj
						staticObj newObj = new staticObj(
							o.name,
							o.transform.position.x,
							o.transform.position.y,
							o.transform.position.z,
							o.transform.rotation.x,
							o.transform.rotation.y,
							o.transform.rotation.z,
							o.transform.lossyScale.x,
							o.transform.lossyScale.y,
							o.transform.lossyScale.z
						);

						// Create new staticObjInstances
						staticObjInstances newObjInstance = new staticObjInstances(
							o.name,
							o.name,
							newObj);

						// Add new object instances to final list
						data.staticObjList.Add(newObjInstance);
						Debug.Log("Added new landscapeObjInstances of " + o.name);
					}
				} 

				// LANDSCAPE OBJECT
				else if (o.GetComponent<ObjType>().type == "landscape") {
					if (data.landscapeObjList.Exists(ele => Regex.IsMatch(Regex.Match(ele.name, @"^()\w+").Value, currentObjectName))) {

						// Create new landscape obj
						landscapeObj newObj = new landscapeObj(
							o.name,
							o.transform.position.x,
							o.transform.position.y,
							o.transform.position.z,
							o.transform.rotation.x,
							o.transform.rotation.y,
							o.transform.rotation.z,
							o.transform.lossyScale.x,
							o.transform.lossyScale.y,
							o.transform.lossyScale.z
						);

						// Add object to existing instances
						data.landscapeObjList.Find(ele => Regex.IsMatch(Regex.Match(ele.name, @"^()\w+").Value, currentObjectName)).instances.Add(newObj);

						Debug.Log("Added new instance of : " + o.name);

					} else {

						// Create new landscape obj
						landscapeObj newObj = new landscapeObj(
							o.name,
							o.transform.position.x,
							o.transform.position.y,
							o.transform.position.z,
							o.transform.rotation.x,
							o.transform.rotation.y,
							o.transform.rotation.z,
							o.transform.lossyScale.x,
							o.transform.lossyScale.y,
							o.transform.lossyScale.z
						);

						// Create new landscapeObjInstances
						landscapeObjInstances newObjInstance = new landscapeObjInstances(
							o.name,
							o.name,
							newObj
						);

						// Add new object instances to final list
						data.landscapeObjList.Add(newObjInstance);
						Debug.Log("Added new landscapeInstances object of " + o.name);
					}
				}
				// INTERACTIVE OBJECT
				else if (o.GetComponent<ObjType>().type == "interactive") {
					interactiveObj newObj = new interactiveObj(
							o.name,
							o.transform.position.x,
							o.transform.position.y,
							o.transform.position.z,
							o.transform.rotation.x,
							o.transform.rotation.y,
							o.transform.rotation.z,
							o.transform.lossyScale.x,
							o.transform.lossyScale.y,
							o.transform.lossyScale.z
						);
					data.interactiveObjList.Add(newObj);
					Debug.Log("Added new interactive object : " + o.name);
				} else {
					// IF WRONG TYPE
					Debug.Log("INVALID OBJECT TYPE " + o.GetComponent<ObjType>().type + " OF OBJECT " + o.name);
				}
			}			
		}
	}

	// Save Data in json file
	void saveData() {
		Debug.Log("SAVING DATA IN JSON FILE ...");
		string content = JsonUtility.ToJson(data, true);
		System.IO.File.WriteAllText(path, content);
		Debug.Log("DATA SAVED !");
	}
}
