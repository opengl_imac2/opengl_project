﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class jsonData 
{
	// List of all interactive objects
	public List<interactiveObj> interactiveObjList = new List<interactiveObj>();

	// List of all static objects
	public List<staticObjInstances> staticObjList = new List<staticObjInstances>();

	// List of all landscape objects
	public List<landscapeObjInstances> landscapeObjList = new List<landscapeObjInstances>();
	
	}


[System.Serializable]
public class Object {
	public string name;
	public float posX;
	public float posY;
	public float posZ;
	public float rotX;
	public float rotY;
	public float rotZ;
	public float scaleX;
	public float scaleY;
	public float scaleZ;
}

[System.Serializable]
public class ObjectInstances {
	public string name;
	public string model;
}

[System.Serializable]
public class interactiveObj : Object {
	public interactiveObj(string _name, float px, float py, float pz, float rx, float ry, float rz, float sx, float sy, float sz) {
		name = _name;
		posX = px;
		posY = py;
		posZ = pz;
		rotX = rx;
		rotY = ry;
		rotZ = rz;
		scaleX = sx;
		scaleY = sy;
		scaleZ = sz;
	}
}

[System.Serializable]
public class staticObj : Object {
	public staticObj(string _name, float px, float py, float pz, float rx, float ry, float rz, float sx, float sy, float sz) {
		name = _name;
		posX = px;
		posY = py;
		posZ = pz;
		rotX = rx;
		rotY = ry;
		rotZ = rz;
		scaleX = sx;
		scaleY = sy;
		scaleZ = sz;
	}
}

[System.Serializable]
public class staticObjInstances : ObjectInstances {
	public List<staticObj> instances = new List<staticObj>();
	public staticObjInstances (string _name, string _model, staticObj firstObj) {
		name = _name;
		model = _model;
		instances.Add(firstObj);
	}
}

[System.Serializable]
public class landscapeObjInstances : ObjectInstances {
	public List<landscapeObj> instances = new List<landscapeObj>();
	public landscapeObjInstances (string _name, string _model, landscapeObj firstObj) {
		name = _name;
		model = _model;
		instances.Add(firstObj);
	}
}

[System.Serializable]
public class landscapeObj : Object {
	public landscapeObj(string _name, float px, float py, float pz, float rx, float ry, float rz, float sx, float sy, float sz) {
		name = _name;
		posX = px;
		posY = py;
		posZ = pz;
		rotX = rx;
		rotY = ry;
		rotZ = rz;
		scaleX = sx;
		scaleY = sy;
		scaleZ = sz;
	}
}
